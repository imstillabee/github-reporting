// Bring Mongoose into the app

let mongoose = require('mongoose'),
    chalk = require('chalk'),
    MongoClient = require('mongodb').MongoClient
const report = require('./report')

const log = console.log
let mongoDb

// Use default
mongoose.Promise = global.Promise

let isConnected = false,
    mongoURI = '',
    mongoOptions = {},
    reportCollectionName = ''

// ObjectId Helper
// let ObjectId = mongoose.Types.ObjectId

let initMongo = (dbURI, options) => {
  // Create the database connection
  log(chalk`{yellow Attempting to connect to Mongo DB @ ${dbURI}}`)

  mongoose.connect(dbURI, options)
  mongoURI = dbURI
  mongoOptions = options
  // mongoClient.connect(dbURI, (err, database) => {
  //   if (err) {
  //     throw err
  //   }
  //
  //   module.exports.mongoDb = database
  //   log.good('Mongo DB Native Driver connected.')
  // })
}

// CONNECTION EVENTS
// When successfully connected
mongoose.connection.on('connected', () => {
  log(chalk`{green Mongoose default connection open to ${mongoURI}}`)
})

// If the connection throws an error
mongoose.connection.on('error', err => {
  log(chalk`{red Mongoose default connection error: ${err}}`)
  isConnected = false
  errorHandler(null, err)
})

// When the connection is disconnected
mongoose.connection.on('disconnected', () => {
  log(chalk`{red Mongoose default connection disconnected}`)
})

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', () => {
  mongoose.connection.close(() => {
    log(chalk`{yellow Mongoose default connection disconnected through app termination}`)
    process.exit(0)
  })
})

let middleware = function (req, res, next) {
  // TODO: After good connection and connection drops, error out to client

  if (mongoose.connection.readyState !== 1) {
    log(chalk`{red Mongo is not connected, attempting connection...}`)

    mongoose.connect(mongoURI)
    .then(conn => {
      if (typeof conn !== 'undefined') {
        if (conn.connected) {
          log(chalk`{green done.}`)
          isConnected = true
          next()
        } else {
          isConnected = false
          errorHandler(res, {
            code: 'NOT_CONNECT',
            message: 'Database connection error'
          })
          next()
        }
      }
    })
    .catch(err => {
      isConnected = false
      errorHandler(res, err)
      next()
    })
  } else next()
}
/* istanbul ignore next */
let errorHandler = (response, RequestError) => {
  let message = '',
      errMsg = RequestError.message,
      code = (/([A-Z])\w+/).exec(errMsg)

  isConnected = false
  switch (code) {
    case 'ECONNREFUSED':
      message = 'Mongo Server unreachable.'
      break
    default:
      message = `Mongo Server error: ${code}`
      break
  }

  log(chalk`{red Mongo Server error: [${code}] ${errMsg}}`)
  if (response !== null) {
    response.status(500).json({
      statusCode: 'ERROR',
      message: message
    })
  }
}

const connectMongoDb = () => MongoClient.connect(mongoURI)

module.exports = {
  init: initMongo,
  mongoose,
  connectMongoDb,
  validateConnectionUp: middleware,
  isConnected: isConnected,
  errorHandler
}

require('./report')
