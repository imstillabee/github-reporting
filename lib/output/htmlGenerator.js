const fs = require('fs'),
      moment = require('moment')

const colors = [
  ['#D52B1E', '#D52B1E'], // red
  ['#ED7000', '#ED7000'], // orange
  ['#FFA700', '#FFA700'], // amber
  ['#FFD500', '#FFD500'], // yellow
  ['#97DC10', '#97DC10'], // lime
  ['#00AC3E', '#00AC3E'], // green
  ['#12CE9B', '#12CE9B'], // turquise
  ['#00BDFF', '#00BDFF'], // sky
  ['#0077FF', '#0077FF'], // blue
  ['#7A13EF', '#7A13EF'], // purple
  ['#F329B5', '#F329B5'], // pink
  ['#F3296E', '#F3296E'], // fuchsia
  ['#666666', '#666666'],
  ['#aaaaaa', '#aaaaaa'],
  ['#D52B1E', '#ffffff'], // red
  ['#ED7000', '#ffffff'], // orange
  ['#FFA700', '#ffffff'], // amber
  ['#FFD500', '#ffffff'], // yellow
  ['#97DC10', '#ffffff'], // lime
  ['#00AC3E', '#ffffff'], // green
  ['#12CE9B', '#ffffff'], // turquise
  ['#00BDFF', '#ffffff'], // sky
  ['#0077FF', '#ffffff'], // blue
  ['#7A13EF', '#ffffff'], // purple
  ['#F329B5', '#ffffff'], // pink
  ['#F3296E', '#ffffff'], // fuchsia
  ['#666666', '#ffffff'],
  ['#aaaaaa', '#ffffff'],
]

let colorIndex = 0

const dateFormat = 'MMM Do YY',
      dateToMomentFormat = 'YYYY/MM/DD',
      labelField = 'emailName'

const htmlGenerator = (dataPoints, saveToFileName, timelineSize) => {
  const template = fs.readFileSync('./html-templates/index.html').toString()

  const startingDate = moment(dataPoints[0].date, dateToMomentFormat)
  let dateLabels = [ startingDate.format(dateFormat) ]

  for (let i = 0; i < timelineSize - 2; i++) {
    const shortDate = startingDate.add(1, 'days').format(dateFormat)

    dateLabels.push(shortDate)
  }
  let datasets = []

  dataPoints.forEach(dataPoint => {
    let developerIdx = datasets.map(ds => ds.label).indexOf(dataPoint[labelField])
    let newDev

    if (developerIdx < 0) {
      if (colorIndex === colors.length)
        colorIndex = 0
      const userColor = colors[colorIndex]

      colorIndex += 1
      newDev = {
        label: dataPoint[labelField],
        data: new Array(timelineSize).fill(0, 0, timelineSize),
        borderColor: userColor[0],
        backgroundColor: userColor[1],
        borderWidth: 2,
        fill: false
      }
      datasets.push(newDev)
      developerIdx = datasets.length - 1
    }

    // ADD DATA POINTS
    const currentDate = moment(dataPoint.date, dateToMomentFormat).format(dateFormat),
          dataIndex = dateLabels.indexOf(currentDate)

    datasets[developerIdx].data[dataIndex] = dataPoint.commits
  })

  let dataSet = {
    labels: dateLabels,
    datasets
  }

  const mergedTemplate = template.replace('##data##', JSON.stringify(dataSet))

  fs.writeFileSync(`${saveToFileName}.html`, mergedTemplate)
}

module.exports = htmlGenerator
