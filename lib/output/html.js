
const outputToHtml = (commitInfo, data) => {
  const { repo, branch, author, committer, stats, fileCount, message } = commitInfo,
        authorName = author.name

  const commitDateTime = new Date(committer.date),
        commitDate = commitDateTime.toLocaleDateString(),
        commitTime = commitDateTime.getTime()
  const colors = [
    '#D52B1E', // red
    '#ED7000', // orange
    '#FFA700', // amber
    '#FFD500', // yellow
    '#97DC10', // lime
    '#00AC3E', // green
    '#12CE9B', // turquise
    '#00BDFF', // sky
    '#0077FF', // blue
    '#7A13EF', // purple
    '#F329B5', // pink
    '#F3296E' // fuchsia
  ]

  let labelIndex = data.labels.indexOf(commitDate)

  if (!data.users[`${authorName}-${repo}`]) {
    const userColor = colors[data.colorIndex]

    data.colorIndex += 1

    data.users[`${authorName}-${repo}`] = {
      label: `${authorName}-${repo}`,
      data: [],
      borderColor: userColor,
      backgroundColor: userColor,
      borderWidth: 2,
      fill: false
    }
  }

  // if you cannot find a match to an existing label, add one
  if (labelIndex < 0) {
    data.labels.push(commitDate)
    Object.keys(data.users).forEach(authorKey => {
      data.users[`${authorKey}-${repo}`].data.push(0)
    })
    labelIndex = data.labels.length - 1
  }

  const dataRow = {
    label: 'Test User',
    data: [2,4,6,8,16],
    backgroundColor: 'yellow',
    borderColor: 'blue',
    borderWidth: 2,
    fill: true
  }

  const users = {
    ['Test User']: {
      data: [],
      backgroundColor: 'yellow',
      borderColor: 'blue',
      borderWidth: 2,
      fill: true
    }
  }



  let newDataPoint

  if (authorName !== committer.name) {
    // This is a merge
    // log(chalk`{bgHex('#0786D5')  {whiteBright ${repo} }}{bgWhite  {purple ᚶ ${branch}} } {gray ${committer.date}} {green ∞} :: Files: {cyan ${fileCount}} {greenBright Merged branch} -- ${authorName}`)
  } else {
    // log(chalk`{bgHex('#0786D5')  {whiteBright ${repo} }}{bgWhite  {black ᚶ ${branch}} } {gray ${committer.date}} {green +${stats.additions}}/{red -${stats.deletions}}={blue ${stats.total}} :: Files: {cyan ${fileCount}} {gray ${message}} -- {yellow ${authorName} }`)

    // const newLog = {
    //   repo,
    //   branch,
    //   date: committer.date,
    //   stats,
    //   fileCount,
    //   message
    // }

    data.users[authorName].data[labelIndex] += 1

    // TODO: FOR USE IN DYNAMIC DATA
    // data.raw.push(commitInfo)
  }
}

module.exports = outputToHtml
