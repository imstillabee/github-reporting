const mongoDataForHtml = (byRepo, byBranch, startDate, endDate) => {
  let aggregateQuery = [
    // Stage 1: IDX: 0
    {
      $project: {
        repo: 1,
        branch: 1,
        authorDate: '$author.date',
        // authorName: '$author.name',
        emailName: '$emailName',
        committerName: '$committer.name',
        fileCount: 1,
        stats: 1,
        message: 1,
        month: { $month: '$author.date' },
        day: { $dayOfMonth: '$author.date' },
        year: { $year: '$author.date' },
        date: { $concat: [ { $substr: [{ $year: '$author.date' }, 0, -1] }, '/', { $substr: [{ $month: '$author.date' }, 0, -1] }, '/', { $substr: [{ $dayOfMonth: '$author.date' }, 0, -1] } ]},
        isSameAuthor: { $strcasecmp: [ '$author.name', '$committer.name' ] }
      }
    },

    // Stage 1.5: IDX: 1
    {
      $match: { isSameAuthor: 0 } // 0 = match = true
    },

    // Stage 2: IDX: 2
    {
      $group: {
        _id: {
          // repo: '$repo',
          // branch: '$branch',
          // author: '$authorName',
          emailName: '$emailName',
          date: '$date',
          year: '$year',
          month: '$month',
          day: '$day',
        },
        commits: { $sum: 1 },
        files: { $sum: '$fileCount' },
        lines: { $sum: '$sum.total' }
      }
    },

    // Stage 3: IDX: 3
    {
      $project: {
        _id: 0,
        repo: '$_id.repo',
        branch: '$_id.branch',
        // author: '$_id.author',
        emailName: '$_id.emailName',
        date: '$_id.date',
        year: '$_id.year',
        month: '$_id.month',
        day: '$_id.day',
        commits: 1,
        files: 1,
        lines: 1
      }
    },

    // Stage 4
    {
      $sort: {
        year: 1,
        month: 1,
        day: 1
      }
    },

  ]

  if (byRepo)
    aggregateQuery[1].$group._id.repo = '$repo'
  if (byBranch)
    aggregateQuery[1].$group._id.branch = '$branch'

  if (startDate && endDate) {
    aggregateQuery[1].$match.$and = [
      { year: { $gte: startDate.get('year') } },
      { month: { $gte: startDate.get('month') + 1 } },
      { day: { $gte: startDate.get('date') } },
      { year: { $lte: endDate.get('year') } },
      { month: { $lte: endDate.get('month') + 1 } },
      { day: { $lte: endDate.get('date') } }
    ]
  } else {
    if (startDate) {
      aggregateQuery[1].$match.year = { $gte: startDate.get('year') }
      aggregateQuery[1].$match.month = { $gte: startDate.get('month') + 1 }
      aggregateQuery[1].$match.day = { $gte: startDate.get('date') }
    }
    if (endDate) {
      aggregateQuery[1].$match.year = { $lte: endDate.get('year') }
      aggregateQuery[1].$match.month = { $lte: endDate.get('month') + 1 }
      aggregateQuery[1].$match.day = { $lte: endDate.get('date') }
    }
  }

  // return new Promise((resolve, reject) => {
  return aggregateQuery
}

module.exports = mongoDataForHtml
