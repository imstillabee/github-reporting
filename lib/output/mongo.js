const mongoose = require('mongoose')
const Report = mongoose.model('Report')
const aliasEmailNames = require('../aliasEmailNames')
let operations = 0

const outputToMongo = (commitInfo, userName, bulkOperator, spinner, noSpinner) => {
  const { _id, repo, branch } = commitInfo

  operations += 1
  spinner.setSpinnerTitle(`%s ${operations}`)

  const emailName = commitInfo.author.email

  commitInfo.emailName = emailName.substr(0, emailName
  .indexOf('@'))
  .replace('.', ' ')
  .replace(/\w\S*/g, txt => `${txt.charAt(0).toUpperCase()}${txt.substr(1).toLowerCase()}`)

  commitInfo.emailName = aliasEmailNames(commitInfo.emailName)

  if (commitInfo.emailName === undefined || commitInfo.emailName === null)
    console.dir(commitInfo)

  if (noSpinner)
    console.log(operations)
  return bulkOperator.find({ _id })
  .upsert()
  .replaceOne(commitInfo)

  // })
  // return Report.findOneAndUpdate({ _id }, { $set: commitInfo }, { upsert: 1 })
  // .then(() => {
  //   operations += 1
  //   spinner.setSpinnerTitle(`%s ${operations}`)
  // })
}

module.exports = outputToMongo
