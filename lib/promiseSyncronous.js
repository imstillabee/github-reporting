const processArrayPromise = (array, processItem) => {
  let results = []

  return array.reduce((aPromise, item) =>
    aPromise
    .then(() =>
      processItem(item)
      .then(data => {
        results.push(data)
        return results
      })
    )
    , Promise.resolve())
}

module.exports = processArrayPromise
