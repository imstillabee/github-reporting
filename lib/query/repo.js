const { repo } = require('../service/git'),
      chalk = require('chalk'),
      promiseWhile = require('../promiseWhile'),
      promiseSyncronous = require('../promiseSyncronous')

const log = console.log

const repoInfo = (repoName, spinner) => {
  let commitsArray = []

  const getCommitPage = ({ sha, pageNumber }) => {
    return repo.commits(repoName, sha, pageNumber)
    .then(commits => {
      let pageCommits = []

      return Promise.all(commits.map(c => {
        // if (c.sha === 'cdc65cec2127d2943f144738fe67a38eba38f328')
        //   console.info('here')

        return repo.commit(repoName, c.sha)
        .then(details => {
          const { commit, stats, files } = details,
                _id = details.commit.tree.sha,
                author = commit.author,
                committer = commit.committer,
                fileCount = files.length || 0

          author.date = new Date(author.date)
          committer.date = new Date(committer.date)
          // log(repoName,'-',sha)
          pageCommits.push({
            _id: `${repoName}-${sha}-${_id}`,
            sha: _id,
            repo: repoName,
            branch: sha,
            author,
            committer,
            fileCount,
            stats,
            message: commit.message,
            dateCollected: new Date()
          })
        })
      }))
      .then(() => {
        if (pageCommits.length > 0) {
          commitsArray = commitsArray.concat(pageCommits)
          return { sha, pageNumber: pageNumber + 1 }
        }
        return { sha, pageNumber: 0 }
      })
    })
  }

  return repo.branches(repoName)
  .then(branches => {
    // branchArray = branches
    log('Found Branches:')
    branches.forEach(branch => {
      log(`${chalk.magenta(`${repoName}/`)}${chalk.green(branch.name)} ${branch.commit.sha}`)
    })

    return branches
  })
  .then(branchArray => {
    const branchCount = branchArray.length
    let currentBranch = 0

    spinner.setSpinnerTitle(`%s Processing branch [${currentBranch}/${branchCount}]`)
    return promiseSyncronous(branchArray, branch => {
      // log(chalk.cyan(branch.name))
      return promiseWhile({
        sha: branch.commit.sha,
        pageNumber: 1
      }, ({ sha, pageNumber }) => pageNumber > 0, getCommitPage)
      .then(() => {
        process.stdout.write(chalk.cyanBright('.'))
        currentBranch += 1
        spinner.setSpinnerTitle(`%s Processing branch [${currentBranch}/${branchCount}]`)
        return commitsArray
      })
      .catch(err => {
        console.dir(err)
        // log(`${chalk.cyanBright(repoName)}/${chalk.cyan(branch.name)} - ${chalk.redBright('*')} ${chalk.red(err.json)}`)
      })
    })
  })
}

module.exports = repoInfo
