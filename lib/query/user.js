const { user } = require('../service/git'),
      chalk = require('chalk')

const log = console.log

let userData = {}

const userInfo = userName =>
  user.get(userName)
  .then(userObject => {
    userData.root = userObject
    log(`Found Match: ${chalk.green(userObject.name)}`)

    return user.starred(userName)
  })
  .then(starredArray => {
    userData.starred = starredArray.map(repo => ({
      id: repo.id,
      name: repo.name,
      fullName: repo.full_name
    }))
    log(`Found ${chalk.blue(userData.starred.length)} Starred Repos.`)

    return userData
  })
  .catch(err => {
    switch (err.status) {
      case 404:
        throw new Error(`No user data found for ${userName}`)
      default:
        throw err
    }
  })


module.exports = userInfo
