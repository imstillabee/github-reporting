const es6Promise = require('es6-promise'),
      chalk = require('chalk')
const log = console.log

es6Promise.polyfill()

require('isomorphic-fetch')

function FetchException (message, endpoint = '', json = {}) {
  this.message = message
  this.name = 'FetchException'
  this.serverResponse = json
  this.endpoint = endpoint
}

let _fetch = (verb = 'GET', endpoint, body) => new Promise((resolve, reject) => {
  let // ws = config.ws, <-- moved to layer above (mockFetch-app-service)
      // token = getToken(),
      headers = {
        'Content-Type': 'application/json; charset=utf-8',
        Accept: 'application/json'
      }

  let options = {
    method: verb,
    mode: 'cors',
    cache: 'default',
    redirect: 'follow',
    timeout: 30000,
    headers: new Headers(headers),
    body: JSON.stringify(body)
  }

  // log(chalk.cyan(`Querying endpoint: ${endpoint}`))

  fetch(endpoint, options)
  .then(response => {
    if (response.status >= 400) {
      throw new FetchException(`Bad response from server: ${response.status} ${response.statusText}`, endpoint, {
        status: response.status,
        statusText: response.statusText,
        ok: response.ok
      })
    }
    return response.json()
  })
  .then(json => {
    if (json.data)
      resolve(json.data)
    else
      resolve(json)
  })
  .catch(err => {
    if (err.message.includes('Bad response from server:'))
      return reject(err)
    if (!err.response)
      err.message = 'Connection to web service timed out.'

    reject(err)
  })
})

module.exports = _fetch
