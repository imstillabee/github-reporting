const mongoDataForHtml = require('./output/mongoDataForHtml'),
      moment = require('moment'),
      chalk = require('chalk'),
      htmlGenerator = require('../lib/output/htmlGenerator'),
      db = require('../models')

const report = db.mongoose.model('Report')

const log = console.log

const callWriteHtml = (spinner, saveToFileName, startDate, endDate, noSpinner) => {
  spinner.setSpinnerTitle('%s Reading Mongo DB data...')
  if (!noSpinner) spinner.start()

  const byRepo = false,
        byBranch = false

  report.aggregate(mongoDataForHtml(byRepo, byBranch, startDate, endDate))
  .exec()
  .then(docs => {
    if (docs.length === 0)
      return log(chalk`{red No records found within}`)

    // massage data into array
    const firstDate = moment(docs[0].date, 'YYYY/MM/DD'),
          lastDate = moment(docs[docs.length - 1].date, 'YYYY/MM/DD')

    const timelineSize = lastDate.diff(firstDate, 'days')

    htmlGenerator(docs, saveToFileName, timelineSize)

    log(chalk`{greenBright Data read completed.}`)
  })
  .catch(err => console.info(err))
  .then(() => {
    log(chalk`{yellow Closing Mongo DB...}`)

    db.mongoose.disconnect()
    log(chalk`{greenBright Mongo DB connection closed.}`)

    spinner.stop()
  })
  .catch(errTwo => console.info('error2', errTwo))
}

module.exports = callWriteHtml
