const promiseWhile = (data, conditionFunction, actionFunction) => {
  const whilst = whilstData => {
    if (conditionFunction(whilstData))
      return actionFunction(whilstData).then(whilst)
    return Promise.resolve(whilstData)
  }

  return whilst(data)
}

module.exports = promiseWhile
