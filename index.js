const pkg = require('./package'),
      chalk = require('chalk'),
      moment = require('moment')
require('./lib/requireText')

const helpText = require('./content/help.txt'),
      main = require('./bin/main')

const log = console.log

log(`GitHub Reporting ${pkg.version}`)

let userName = '',
    saveToFileName = 'github-report',
    gitHubApiServer = 'https://api.github.com',
    mongoURI = '',
    outputCsv = false,
    outputScreen = false,
    outputMongo = false,
    outputHtml = false,
    readOnly = false,
    noSpinner = false,
    startDate,
    endDate

if (process.argv.length > 2) {
  process.argv.forEach((arg, idx) => {
    switch (arg) {
      case '?':
      case '-?':
      case '--help':
        log(chalk`${helpText}`)
        process.exit()
        break
      case 'f':
      case '-f':
      case '--file':
        saveToFileName = process.argv[idx + 1]
        break
      case 's':
      case '-s':
      case '--server':
        gitHubApiServer = process.argv[idx + 1]
        break
      case 'u':
      case '-u':
      case '--user':
        userName = process.argv[idx + 1]
        break
      case '--csv':
        outputCsv = true
        break
      case '--start':
        startDate = moment(process.argv[idx + 1], 'YYYY/MM/DD')
        break
      case '--end':
        endDate = moment(process.argv[idx + 1], 'YYYY/MM/DD')
        break
      case '--mongo':
        outputMongo = true
        mongoURI = process.argv[idx + 1]
        break
      case '--html':
        outputHtml = true
        break
      case '--readOnly':
        readOnly = true
        break
      case '--noSpinner':
        noSpinner = true
        break
      case '--screen':
        outputScreen = true
        break
      default:
        break
    }
  })

  // Now logic based on params
  if (!userName) {
    log('Expects username: "-u <username>"')
    process.exit()
  }

  main(userName, { outputScreen, outputCsv, outputMongo, outputHtml, readOnly, startDate, endDate, noSpinner }, saveToFileName, mongoURI)
} else
  log('Pass in your user name as the first parameter: -u <username>')
