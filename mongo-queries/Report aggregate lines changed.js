db['github-report-v530737'].aggregate(

	// Pipeline
	[
		// Stage 1
		{
			$project: {
			    // specifications
			    repo:1,
			    branch: 1,
			    authorDate: "$author.date",
			    authorName: "$author.name",
			    committerName: "$committer.name",
			    fileCount: 1,
			    delta: "$stats.total",
			    message: 1,
			    month: { $month: '$author.date' },
			    day:  { $dayOfMonth: '$author.date' },
			    year:  { $year: '$author.date' },
			    date: { $concat: [ { $substr: [{ $year: '$author.date' }, 0, -1] }, '/', { $substr: [{ $month: '$author.date' }, 0, -1] }, '/', { $substr: [{ $dayOfMonth: '$author.date' }, 0, -1] } ]},
			    isSameAuthor: { $strcasecmp: [ "$author.name", "$committer.name" ] }
			}
		},

		// Stage 2
		{
			$group: {
				_id: { 
				  //repo: "$repo", 
			      //branch: "$branch",
			      author: '$authorName',
			     
			      date: '$date',
			      year: '$year',
			      month: '$month',
			      day: '$day',
				},
				delta: { $sum: '$delta' }
			}
		},

		// Stage 3
		{
			$project: {
			    _id: 0,
			    repo: '$_id.repo',
			    branch: '$_id.branch',
			    author: '$_id.author',
			    date: '$_id.date',
			    year: '$_id.year',
			    month: '$_id.month',
			    day: '$_id.day',
			    delta: 1
			}
		},

		// Stage 4
		{
			$sort: {
				year: 1,
				month: 1,
				day: 1
			}
		},

	]

	// Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

);
